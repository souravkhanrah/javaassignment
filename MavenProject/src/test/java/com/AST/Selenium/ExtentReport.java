package com.AST.Selenium;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentReport {
	String ResultFilePath;
	ExtentReports report;
	ExtentTest test;
	
	public ExtentReport(String filePath) {
		this.ResultFilePath=filePath;
		report=new ExtentReports(this.ResultFilePath);
	}
	public void startTest(String testName) {
		test=report.startTest(testName);
	}
	public void info(String msg) {
		test.log(LogStatus.INFO, msg);
	}
	public void pass(String msg) {
		test.log(LogStatus.PASS, msg);
	}
	public void fail(String msg) {
		test.log(LogStatus.FAIL, msg);
	}
	public void endTest() {
		report.endTest(test);
	}
	public void flush() {
		report.flush();
	}
}
