package com.AST.Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Browser {
	WebDriver driver;
	public void init() {
		init("chrome");
		driver.manage().window().maximize();
	}
	public void init(String browserType) {
		if(browserType.equalsIgnoreCase("chrome"))
		{
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
		}else if(browserType.equalsIgnoreCase("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver=new FirefoxDriver();	
		}else if(browserType.equalsIgnoreCase("edge")) {
			WebDriverManager.edgedriver().setup();
			driver=new EdgeDriver();
		}
	}
	public void navigateTo(String url) {
		driver.get(url);
	}
	public void sendText(String id,String text) {
		driver.findElement(By.id(id)).sendKeys(text);
	}

	public void clickXP(String id) {
		driver.findElement(By.id(id)).click();;
	}
	public void clickAddToCartByXP(String xp) {
		driver.findElement(By.xpath(xp)).click();
	}
	public void clickDropdownByXP(String xp,String val) {
		WebElement ele=driver.findElement(By.xpath(xp));
		Select sel=new Select(ele);
		sel.selectByValue(val);
	}
	
	// for Thank U page to check if complete page arise or not
	public boolean exist(String xp) {
		boolean res=false;
		try {
			WebElement ele=driver.findElement(By.xpath(xp));
			ele.isDisplayed();
			res=true;
		} catch (Exception e) {
			
		}
		return res;
	}
	public void close() {
		driver.close();
	}
}
