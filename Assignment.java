package com.astAssignmentJava;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Assignment {
  //## fibonacci series
	public static void fibonacci(int n) {
		int first=0;
		int second=1;
		int fibo;
		System.out.print(first+" "+second);
		for(int i=2;i<n;i++) {
			fibo=first+second;
			first=second;
			second=fibo;
			System.out.print(" "+fibo);
		}
	}
	//## factorial of a given number
	public static int factorial(int n) {
		if(n==0 || n==1) {
			return 1;
		}else if(n<0) {
			 throw new IllegalArgumentException ("n must be non-negative");
		}
		else {
		  return n*factorial(n-1);
		}
	}
   // ## swap two numbers	
   public static void swap(int a,int b) {
	   System.out.println("Before swap value of a and b : "+a+" "+b);
	   a=a+b;
	   b=a-b;
	   a=a-b;
	   System.out.println("After swap value of a and b : "+a+" "+b);
   }
   // ## largest of three numbers using ternary operator
   public static void largestNo(int a,int b,int c) {
	   int largest=(a > b) ? (a > c ? a : c) : (b > c ? b : c);  
	   System.out.println("Largest of Three number is :"+largest);
   }
   //## Leapyear
   public static void leapyear(int year) {
	   if((year%100==0 && year%400==0) || (year%100!=0 && year%4==0)) {
		   System.out.println("The given year is leap year");
	   }else {
		   System.out.println("The year is not leap year");
	   }
   }
   
   // ## find the reverse no
   public static void reverseNo(int num) {
	   int n=num;
	   int rev=0,rem;
	   while(n!=0) {
		   rem=n%10;
		   rev=rev*10+rem;
		   n/=10;
	   }
	   System.out.println("Reverse of "+num+" is = "+rev);
   }
   // ## sum of array elements
   public static void sumofArrayele(int arr[]) {
	int sum=0;
	for(int i=0;i<arr.length;i++) {
		sum=sum+arr[i];
	}
	System.out.println("Sum of Array elements is : "+sum);
   }
   
   // ## prime or not
   public static void prime(int num) {
		int count = 0;
		if (num == 0 || num == 1) {
			System.out.println(num + " is not prime number");

		} else {
			for (int i = 2; i < num; i++) {
				if (num % i == 0) {
					count++;
					break;
				}
			}
		
			if (count == 0)
				System.out.println("The given number is prime..");
			else
				System.out.println("The given number is not prime..");
		} 
   }
   //## odd even
	public static void odd_even(int num) {
		if (num < 0) {
			System.out.println("negative is not allowed");
		} else if (num % 2 == 0) {
			System.out.println("Number " + num + " is Even..");
		} else {
			System.out.println("Number " + num + " is Odd..");
		}
	}
	
 // Generating random number
	public static void randomNo(int n) {
		Random ran = new Random(); 
		for(int i=0;i<n;i++) {
			int ranNO=ran.nextInt(50);
			System.out.println("Random No : " + ranNO); 
			
			
		}
	}
// palindrome or not in a given string
   public static void palindrome(String str) {
	   String rev="";
	   for(int i=str.length()-1;i>=0;i--) {
		   rev=rev+str.charAt(i);
	   }
	   System.out.println("Reverse of "+str+" is: "+rev);
	   if(str.equals(rev)) {
		   System.out.println(str+" is palindrome...");
	   }else {
		   System.out.println(str+" is not palindrome...");
	   }
   }
   // n-th largest number in a given array
   
   public static void nthlargest(int arr[],int k) {
	   int n=arr.length;
	   int temp;
	   for(int i=0;i<n;i++) {
		   for(int j=i+1;j<n;j++) {
			   if(arr[i]<arr[j]) {
				   temp = arr[i];  
                   arr[i] = arr[j];  
                   arr[j] = temp;  
			   }
		   }		  
		   if(i==k-1) {
			   System.out.println(k+"- largest number: "+arr[i]);
			   break;
		   }
	   }
	   
   }
   //Reverse a string without reverse function
   public static void reverseStr(String str) {
	   String rev="";
	   for(int i=str.length()-1;i>=0;i--) {
		   rev=rev+str.charAt(i);
	   }
	   System.out.println("Reverse of "+str+" is: "+rev);
	  
   }
   // Sort numbers in an array
   
   public static void arraySort(int arr[],int n) {
	   int temp=0;
	   int i;
	   System.out.println("Before Sort...");  
       for(i=0; i < arr.length; i++){  
               System.out.print(arr[i] + " ");  
       }  
	   for(i=0;i<n;i++) {
		   for(int j=1;j<n-i;j++) {
			   if(arr[j-1]>arr[j]) {
				   temp=arr[j-1]; 
				   arr[j-1]=arr[j];
				   arr[j]=temp;
			   }
		   }
	   }
	    System.out.println();  
	    System.out.println("After Sort ....");  
        for(i=0; i < arr.length; i++){  
            System.out.print(arr[i] + " ");  
        } 
        
   }
   
   //To print each unique string and how many times its appear
   public static Map<String,Integer> getuniqueString(String[] word) {
	   Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < word.length; i++) {
			int occNo = 0;
			for (int j = 0; j < word.length; j++) {
				if (word[i].equals(word[j])) {
					occNo++;
					map.put(word[i], occNo);
				}
			}
		}
		return map;
//		for (Map.Entry<String, Integer> m : count.entrySet()) {
//			System.out.println("\n"+m.getKey() + " " + m.getValue());
//		}
		  
   }
   public static void nthNumberRegExp(String str,int n) {
	   Pattern p = Pattern.compile("[0-9]+");
       Matcher m = p.matcher(str);
       int occ=0;
       while(m.find()) {
    	   occ++;
    	   if(occ==n) {
           System.out.println(m.group());
    	   }
       }
	}
   
	public static void main(String[] args) {
		fibonacci(6);
		try {
			int res = factorial(5);
			System.out.println("\nfactorial= "+res);
		} catch (IllegalArgumentException e) {
			System.out.println("\n"+e);
		}
		swap(10,20);
		largestNo(15,20,8);
		leapyear(2021);
		reverseNo(124);
		int arr[]= {1,2,3,4,5,2};
		sumofArrayele(arr);
		prime(26);
		//odd_even(10);
		randomNo(10);
		palindrome("naran");
		int a[]= {5,80,12,7,6,2,4};
		nthlargest(a,2);
		
		 //reverseStr("sourav");
		// int arr[]= {2,10,15,-2,20,12}; 
		 int n = arr.length;
		//arraySort(arr,n);
		//======using return of map
		
		String[] word= {"one","three","two","four","one","three","four"};
		Map<String,Integer> res=getuniqueString(word);	
		for (Map.Entry<String, Integer> m : res.entrySet()) {
			System.out.println("\n"+m.getKey() + " " + m.getValue());
		}
		
		// String str="Your order 1234 is placed successfully in 255 Block";
		 //nthNumberRegExp(str,1);
		}
}
