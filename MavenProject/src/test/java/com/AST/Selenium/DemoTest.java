package com.AST.Selenium;

import app.pages.CheckoutPage;
import app.pages.CompletePage;
import app.pages.ProductsPage;
import app.pages.YourCartPage;
import app.pages.YourInformationPage;
import library.Excel;

public class DemoTest {

	public static void main(String[] args) {
		ExtentReport r=new ExtentReport("D:\\DemoTest.html");
		r.startTest("DemoTest");
		Browser b=new Browser();
		
		Excel ex=new Excel();
		String username="";
		String password="";
		username=ex.getUserName();
		password=ex.getPassword();
		
		
		b.init();
		b.navigateTo("https://www.saucedemo.com/");
		b.sendText("user-name", username);
		r.info("username is entered successfully!!");
		b.sendText("password", password);
		r.info("password is entered successfully!!");
		//b.sendText("user-name", "standard_user");
		//b.sendText("password","secret_sauce");
		b.clickXP("login-button");
		
		String addToCartXP=ProductsPage.getAddToCartXP("Sauce Labs Backpack");
		System.out.println("Xpath of 'Sauce Labs Backpack' -->"+addToCartXP);
		b.clickAddToCartByXP(addToCartXP);
		
		//Sauce Labs Bolt T-Shirt
		String addToCartXP1=ProductsPage.getAddToCartXP("Sauce Labs Bolt T-Shirt");
		System.out.println("Xpath of 'Sauce Labs Bolt T-Shirt' -->"+addToCartXP1);
		b.clickAddToCartByXP(addToCartXP1);
		
		//select drodown
		String dropdownXP=ProductsPage.getDropdownXP();
		b.clickDropdownByXP(dropdownXP,"hilo");
		
		r.info("Selected value in drop down");
		
		//click on Shopping cart icon
		b.clickAddToCartByXP(ProductsPage.getAddToCartIconXP());
		//navigate to cart page and click on checkout
		String checkoutId=YourCartPage.checkoutID();
		b.clickXP(checkoutId);
		
		//moved to your information page and click on continue
		String firstNameId=YourInformationPage.getFirstNameID();
		b.sendText(firstNameId, "Anit");
		String lastNameId=YourInformationPage.getLastNameID();
		b.sendText(lastNameId, "Maity");
		String zipId=YourInformationPage.getZipId();
		b.sendText(zipId, "711411");
		String continueId=YourInformationPage.continueBtnID();
	
		b.clickXP(continueId);
		//land to overview page and click on finish
		String finishId=CheckoutPage.finishID();
	
		b.clickXP(finishId);
		//then Thank You Page
		if(b.exist(CompletePage.thankYouMsgXP())) {
			System.out.println("Scenerio successfully executed..");
			r.pass("Welcome !! Test case is Passed successfully..");
		}else {
			System.out.println("Scenerio failed..");
			r.fail("Sorry !! Test case is not passed successfully");
		}
		
		r.endTest();
		r.flush();
		
		//b.close();

	}

}
